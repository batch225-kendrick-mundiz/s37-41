const express = require("express");
const router = express.Router();
const auth = require("../auth");

const userController = require("../controllers/userController");

// Check Email 
router.post("/checkEmail", (req, res) =>{
    userController.checkEmailExists(req.body)
    .then(result => res.send(result));
})

router.post("/register", (req, res) =>{
    userController.registerUser(req.body)
    .then(result => res.send(result));
})

router.get("/details/:id", (req, res) =>{
    userController.getDetails(req.params.id)
    .then(result => res.send(result));
})

router.post("/login", (req, res) => {
    userController.loginUser(req.body)
    .then(result => res.send(result))
})

router.post("/enroll", auth.verify, (req, res) => {
    let data = {
        //get user logged in id from jwt
        userId : auth.decode(req.headers.authorization).id,

        //course id you want to enroll
        courseId : req.body.courseId
    }

    userController.enroll(data)
    .then(result => res.send(result))
})

//export router
module.exports = router;