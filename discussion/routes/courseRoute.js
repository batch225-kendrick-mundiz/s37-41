const express = require("express");
const router = express.Router();
const auth = require("../auth");

const courseController = require("../controllers/courseController");


//get all courses
router.get("/", (req, res) =>{
    return courseController.getAllCourses()
    .then(result => res.send(result))
});

//router
//verify if token exists
//call auth.verify in post arguements
router.post("/addcourse", auth.verify, (req, res) => {

    const data =  {
        course : req.body,
        //asign isadmin from token to isadmin 
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }

    courseController.addCourse(data)
    .then(result => res.send(result))
});

//route for updating courses
router.put("/:courseId", auth.verify, (req, res) => {
    courseController.updateCourse(req.params, req.body)
    .then(result => res.send(result))
})

//archive 
router.put("/archive/:courseId", auth.verify, (req, res) => {
    courseController.archiveCourse(req.params)
    .then(result => res.send(result))
})

router.get("/getAllActive", auth.verify, (req, res) => {
    courseController.getAllActive()
    .then(result => res.send(result))
})

module.exports = router;