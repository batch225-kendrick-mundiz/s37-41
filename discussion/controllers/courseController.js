const { update } = require("../models/course");
const Course = require("../models/course");

module.exports.addCourse = (data) => {

    if(data.isAdmin){
		let new_course = new Course ({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		})

		return new_course.save().then((new_course, error) => {
			if(error){
				return {
                    message : error
                }
			}
			return {
				message: `${new_course.name} successfully created!`
			}
		})
	} 

    //if promise is fulfilled, this code will not run
	let message = Promise.resolve('User must be Admin to add course.')

    //if something is assigned to message, or message is assigned the promised value, then the string assigned to message will be returned
	return message.then((value) => {
		return value
	});
		
}

module.exports.getAllCourses = () => {
    return Course.find({})
    .then(result => {
        if(result.length > 0){
            return result
        }else{
            return {
                message : "Add a course first"
            }
        }
    })
}

module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse)
	.then((course, error) => {
		if (error) {
			return false
		} else {
			return {
				message : `${course.name} course has been updated`
			}
		}
	})
};

module.exports.archiveCourse = (reqParams) => {
	let updatedCourse = {
		isActive : false
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse)
	.then((course, error) => {
		if (error) {
			return {
				message : error
			}
		} else {
			return {
				message : `${course.name} course has been archived`
			}
		}
	})
}

module.exports.getAllActive = () => {
	return Course.find({isActive :true})
	.then(result => {
		if(result.length > 0){
			return result
		}else {
			return {message : "No active courses"}
		}
	})
}
