const User = require("../models/user");
const Course = require("../models/course");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const { exists, db } = require("../models/user");



module.exports.checkEmailExists = (reqBody) => {

    return User.find({ email : reqBody.email })
    .then(result => {
        if (result.length > 0){
            return {
                message: "User was found"
            }
        }else if(result.lenth > 1){
            return {
                message: "Duplicate User"
            }
        }else {
            return {
                message: "User not found"
            }
        }
    });
}

module.exports.registerUser = (reqBody) => {

    let newUser = new User({
        firstName : reqBody.firstName,
        lastName : reqBody.lastName,
        userName : reqBody.userName,
        email : reqBody.email,
        mobileNo : reqBody.mobileNo,
        password : bcrypt.hashSync(reqBody.password, 10) 
    });

    return newUser.save().then((user, error) => {
        if (error) {
            return false;
        }else{
            return user;
        }
    });
}

module.exports.getDetails = (userId) => {
    return User.findById(userId)
    .then(result => {
        result.password = "";
        return result
    }).catch(error => {
        return {
            message : "User not found"
        }
    });
};


//user authentication

/*
steps:
check the db if user email exists
compare password provided in the login form with the password stored in the db
generate/return a json web token if the user has successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {
    return User.findOne({ email : reqBody.email})
    .then(result => {
        if(result ==null){
            return {
                message : "Not found in our database"
            }
        }else{
            // The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

            if(isPasswordCorrect){
                return { access : auth.createAccessToken(result)}
            }else{
                return {
                    message : "password was incorrect"
                }
            }
        }
    })
}

module.exports.enroll = async (data) => {
    let isUserUpdated = await User.findById(data.userId)
    .then(user => {
        user.enrollments.push({courseId : data.courseId})

        return user.save().then((user, error) => {
            if (error){
                return false
            } else {
                return "Successfully enrolled";
            }
        })
    });

    // Add the user ID in the enrollees array of the course
	// Using the "await" keyword will allow the enroll method to complete updating the course before returning a response back to the frontend
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		// Adds the userId in the course's enrollees array
		course.enrollees.push({userId : data.userId});
        // Saves the updated course information in the database
		return course.save().then((course, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});

	});

    return {isCourseUpdated, isUserUpdated}
}