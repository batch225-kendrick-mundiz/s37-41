// setup dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
require("dotenv").config();


//server setup
const app = express();
const port = 3010;
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//routes
const userRoutes = require("./routes/userRoutes");
const courseRoute = require("./routes/courseRoute");

//database connection
mongoose.connect(
  `mongodb+srv://kendrick123:${process.env.MONGODB_PASSWORD}@cluster0.nkwttx3.mongodb.net/test2?retryWrites=true&w=majority`,
  { useNewUrlParser: true, useUnifiedTopology: true }
);

let db = mongoose.connection;
//error handling
db.on("error", console.error.bind(console, "Conn error"));
db.on("open", () => console.log("Connected to database"));


//call routes
app.use("/users", userRoutes);
app.use("/courses", courseRoute);


app.listen(port, ()=> console.log(`Server listening at port: ${port}`));