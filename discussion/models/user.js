const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	firstName: {
        type: String,
        required: [true, "First name is required"]
    },
	lastName: {
        type: String,
        required: [true, "Last name is required"]
    },
	userName: {
        type: String,
        required: [true, "Username is required"]
    },
	password: {
        type: String,
        required: [true, "Password is required"]
    },
	email: {
        type: String,
        required: [true, "Email is required"]
    },
	isAdmin: {
        type: Boolean,
        default: false
    },
    mobileNo: {
        type: String,
        required: [true, "Mobile number is required"]
    },
    enrollments: [
        {
            coursId : String,
            enrolledOn : {
                type: Date,
                default : new Date(+new Date() + 7*24*60*60*1000),
                status: {
                    type: String,
                    default: "Enrolled"
                }
            }
        }
    ],

});

module.exports = mongoose.model("User", userSchema);